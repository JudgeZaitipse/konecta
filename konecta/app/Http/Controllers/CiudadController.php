<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCiudadRequest;
use App\Http\Requests\UpdateCiudadRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Ciudad;
use Carbon\Carbon;
use Response;
use Flash;
class CiudadController extends Controller{
	/** @var  CiudadRepository */
	public function __construct(){
		$this->middleware('auth');
		$this->middleware('ability:admin,mostrar_ciudad', ['only' => ['index']]);
		$this->middleware('ability:admin,crear_ciudad', ['only' => ['store']]);
		$this->middleware('ability:admin,editar_ciudad', ['only' => ['update']]);
		$this->middleware('ability:admin,exportar_ciudad', ['only' => ['reporteCiudades']]);
	}

    /**
     * Display a listing of the Ciudad.
     *
     * @param Request $request
     * @return Response
     */
    public function index(){
    	$ciudades = Ciudad::all();
    	return view('ciudades.index',compact('ciudades'));
    }

    public function ciudadesIndex(Request $request){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $columns = array(
            0 => 'id',
            1 => 'nombre',
            2 => 'action'
        );

        $totalData = Ciudad::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $posts = Ciudad::offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
            $totalFiltered = Ciudad::count();
        }else{
            $search = $request->input('search.value');
            $posts = Ciudad::where('id', 'like', "%{$search}%")
            ->orWhere('nombre','like',"%{$search}%")
            ->orWhere('created_at','like',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();                                
            $totalFiltered = Ciudad::where('id', 'like', "%{$search}%")
            ->orWhere('nombre','like',"%{$search}%")
            ->count();
        }       

        $data = array();

        if($posts){
            foreach($posts as $r){
                $nestedData['id'] = $r->id;
                $nestedData['nombre'] = $r->nombre;
                $nestedData['action'] = $r;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        return json_encode($json_data);  
    }
    /**
     * Store a newly created Ciudad in storage.
     *
     * @param CreateCiudadRequest $request
     *
     * @return Response
     */
    public function store(CreateCiudadRequest $request){
    	DB::beginTransaction();
    	try{
    		$ciudad = new Ciudad;
    		$ciudad->nombre = $request->nombre_ciudad;
    		$ciudad->save();
    		DB::commit();
    		return 1;
    	}catch (\Throwable $th) {
    		$success = false;
    		$error = $th->getMessage();
    		dd($error);
    		DB::rollback();
    		return $error;
    	}
    }

    /**
     * Update the specified Ciudad in storage.
     *
     * @param  int              $id
     * @param UpdateCiudadRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCiudadRequest $request){
    	DB::beginTransaction();
    	try{
    		$ciudad = Ciudad::find($id);
    		$ciudad->nombre = $request->nombre_ciudad_edit;
    		$ciudad->save();
    		DB::commit();

    	}catch (\Throwable $th) {
    		$success = false;
    		$error = $th->getMessage();
    		dd($error);
    		DB::rollback();
    		return $error;
    	}
    }
    public function destroy($id){
    	$ciudad = Ciudad::find($id);
    	if(!empty($ciudad)){
    		$ciudad->delete();
    		return 1;
    	}else{
    		return 0;
    	}
    }
}
