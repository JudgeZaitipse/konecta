<?php

namespace App\Http\Controllers;

use App\Models\registro;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ClienteImport;

class RegistroController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ability:admin,mostrar_clientes', ['only' => ['index']]);
        $this->middleware('ability:admin,mostrar_clientes', ['only' => ['store','index']]);
        $this->middleware('ability:admin,mostrar_clientes', ['only' => ['update']]);
        $this->middleware('ability:admin,mostrar_clientes', ['only' => ['destroy']]);
    }

    public function index()
    {
        $registro = registro::all();
        return view('Registros.index', compact('registro'));
    }

    public function store(Request $request)
    {

        $registro               = new registro;
        $registro->nombres      = $request->nombres;
        $registro->cedula       = $request->cedula;
        $registro->email        = $request->email;
        $registro->celular      = $request->celular;
        $registro->cargo        = $request->cargo;
        if ($registro->save()) {
            return response()->json(['success' => $registro]);
        } else {
            return 0;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function show(registro $registro)
    {
        $user = User::where('username', $data->username)->first();
        if (!empty($user)) {
            $mensaje = 'El nombre ya ha sido registrado';
            $codigo  = 419;
        } else {
            $mensaje = 'Nombre sin registrar';
            $codigo  = 200;
        }
        return response($mensaje, $codigo);
    }

    public function indexMasivo()
    {
        return view('Registros.masivo');
    }
    public function insercionMasiva(Request $request)
    {
        $file = $request->file('archivo');
        Excel::import(new ClienteImport, $file);

        return back()->with('message', 'Importanción de usuarios completada');
    }

    public function update(Request $request)
    {
        $registro               = registro::find($request->id_act);
        $registro->nombres      = $request->nombres_act;
        $registro->cedula       = $request->cedula_act;
        $registro->email        = $request->email_act;
        $registro->celular      = $request->celular_act;
        $registro->cargo        = $request->cargo_act;
        if ($registro->update()) {
            return response()->json(['success' => $registro]);
        } else {
            return 0;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\registro  $registro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id       = $request->id;
        $registro = registro::find($id);
        $registro->delete();
        return [
            'status' => true,
        ];
    }
}
