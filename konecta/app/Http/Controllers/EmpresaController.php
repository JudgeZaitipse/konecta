<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEmpresaRequest;
use App\Http\Requests\UpdateEmpresaRequest;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use App\Exports\EmpresaExport;
use Illuminate\Http\Request;
use App\Models\Empresa;
use App\Models\Ciudad;
use Carbon\Carbon;
use Response;
use Flash;
class EmpresaController extends Controller{
    /** @var  EmpresaRepository */
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('ability:admin,mostrar_empresa', ['only' => ['index']]);
        $this->middleware('ability:admin,crear_empresa', ['only' => ['store']]);
        $this->middleware('ability:admin,editar_empresa', ['only' => ['update']]);
        $this->middleware('ability:admin,exportar_empresa', ['only' => ['reporteEmpresas']]);
    }

    /**
     * Display a listing of the Empresa.
     *
     * @param Request $request
     * @return Response
     */
    public function index(){
        $empresas = Empresa::all();     
        $ciudades = Ciudad::pluck('nombre','id');
        return view('empresas.index',compact('empresas','ciudades'));
    }

    public function empresasIndex(Request $request){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $columns = array(
            0 => 'id',
            1 => 'nombre',
            2 => 'nit',
            3 => 'direccion',
            4 => 'telefono',
            5 => 'ciudad_id',
            6 => 'observaciones',
            7 => 'action'
        );

        $totalData = Empresa::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        
        if(empty($request->input('search.value'))){
            $posts = Empresa::with('ciudad')
            ->offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
            $totalFiltered = Empresa::count();
        }else{
            $search = $request->input('search.value');
            $posts = Empresa::with('ciudad')
            ->where('nombre', 'like', "%{$search}%")
            ->orWhere('nit','like',"%{$search}%")
            ->orWhere('direccion','like',"%{$search}%")
            ->orWhere('telefono','like',"%{$search}%")
            ->orWhere('ciudad_id','like',"%{$search}%")
            ->orWhere('observaciones','like',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();                                
            $totalFiltered = Empresa::where('nit', 'like', "%{$search}%")
            ->orWhere('nombre','like',"%{$search}%")
            ->count();
        }       

        $data = array();
        
        if($posts){
            foreach($posts as $r){
                $nestedData['nombre'] = $r->nombre;
                $nestedData['nit'] = $r->nit;
                $nestedData['direccion'] = $r->direccion;
                $nestedData['telefono'] = $r->telefono;
                $nestedData['ciudad_id'] = $r->ciudad->nombre;
                $nestedData['observaciones'] = $r->observaciones;
                $nestedData['action'] = $r;
                $data[] = $nestedData;
            }
        }
        
        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        return json_encode($json_data);  
    }
    /**
     * Store a newly created Empresa in storage.
     *
     * @param CreateEmpresaRequest $request
     *
     * @return Response
     */
    public function store(CreateEmpresaRequest $request){
        DB::beginTransaction();
        try{
            $empresa = new Empresa;
            $empresa->nombre = $request->nombre_empresa;
            $empresa->nit = $request->nit_empresa;
            $empresa->direccion = $request->direccion_empresa;
            $empresa->telefono = $request->telefono_empresa;
            $empresa->ciudad_id = $request->ciudad_empresa;
            $empresa->estado = 1;
            $empresa->telefono = $request->telefono_empresa;
            $empresa->observaciones = $request->observaciones_empresa;
            $empresa->save();
            DB::commit();
            return 1;
        }catch (\Throwable $th) {
            $success = false;
            $error = $th->getMessage();
            dd($error);
            DB::rollback();
            return $error;
        }
    }

    /**
     * Update the specified Empresa in storage.
     *
     * @param  int              $id
     * @param UpdateEmpresaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmpresaRequest $request){
        DB::beginTransaction();
        try{
            $empresa = Empresa::find($id);
            if(!empty($empresa)){
                $empresa->nombre = $request->nombre_empresa_edit;
                $empresa->nit = $request->nit_empresa_edit;
                $empresa->direccion = $request->direccion_empresa_edit;
                $empresa->telefono = $request->telefono_empresa_edit;
                $empresa->ciudad_id = $request->ciudad_empresa_edit;
                $empresa->telefono = $request->telefono_empresa_edit;
                $empresa->observaciones = $request->observaciones_empresa_edit;
                $empresa->save();
                DB::commit();
                return 1;
            }else{
                return 0;
            }
        }catch (\Throwable $th) {
            $success = false;
            $error = $th->getMessage();
            dd($error);
            DB::rollback();
            return $error;
        }
    }
    public function reporteEmpresas(Request $request){
        $fechaActual = Carbon::now()->toDateTimeString();
        return Excel::download(new EmpresaExport($request), 'reporteEmpresas_'.$fechaActual.'.xlsx');
    }
}
