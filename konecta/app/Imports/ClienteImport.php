<?php

namespace App\Imports;

use App\Models\registro;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use DB;

class ClienteImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $key =>  $value) 
        {
            if ($key !=0) {
            	if ($value[0] !=null || $value[0] != '') {
            		registro::create([
            			'nombres'         => $value[1],
            			'cedula'          => $value[0],
            			'email'           => $value[2],
            			'celular'         => $value[3],
            			'cargo'           => $value[4],
            		]);
            	}
            }
        }
    }
}
