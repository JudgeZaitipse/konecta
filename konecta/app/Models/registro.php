<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class registro extends Model
{
    protected $table    = 'registros';
    protected $fillable = ['nombres', 'cedula', 'celular', 'email', 'cargo'];
    protected $guarded  = ['id'];
}
