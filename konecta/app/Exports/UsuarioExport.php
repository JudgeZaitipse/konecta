<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;
class UsuarioExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
	public $datos;
	public function __construct($datos){
		$this->datos = $datos;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
	public function headings(): array
	    {
	        return [
        'Id',
        'Nombre',
        'Email',
        'Nombre de Usuario',
        'Cargo',
        'Empresa',
        'Estado',
        'Fecha de Creación',
        'Fecha de Actualización',
	     ];
	    }

	public function map($row): array
	    {
            $empresa = DB::table('empresas')->where('id', $row->empresa_id)->pluck('nombre');
            
            if ($row->estado == 1) {
                $estado = 'Activo';
            }else{   
                $estado = 'Inactivo';
            }

	        return [
	            $row->id,
	            $row->name,
	            $row->email,
	            $row->username,
	            $row->cargo,
	            $empresa[0],
	            $estado,
	            $row->created_at,
	            $row->updated_at
	        ];
	    }

    public function title(): string
    {
    	$request = $this->datos;

                return 'Usuario';
    }

    public function collection()
    {
    	$request = $this->datos;
        $reporte = User::all();
        return $reporte;
    }
}
