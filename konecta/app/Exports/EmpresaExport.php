<?php

namespace App\Exports;

use App\Models\Empresa;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Database\Query\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;
class EmpresaExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
	public $datos;
	public function __construct($datos){
		$this->datos = $datos;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
	public function headings(): array
	    {
	        return [
        'Id',
        'Nombre',
        'Nit',
        'Dirección',
        'Ciudad',
        'Estado',
        'Teléfono',
        'Observaciones',
        'Fecha de Creación',
        'Fecha de Actualización',
	     ];
	    }

	public function map($row): array
	    {
            $ciudad = DB::table('ciudades')->where('id', $row->ciudad_id)->pluck('nombre');
            
            if ($row->estado == 1) {
                $estado = 'Activo';
            }else{   
                $estado = 'Inactivo';
            }

	        return [
	            $row->id,
	            $row->nombre,
	            $row->nit,
	            $row->direccion,
	            $ciudad[0],
	            $estado,
	            $row->telefono,
	            $row->observaciones,
	            $row->created_at,
	            $row->updated_at
	        ];
	    }

    public function title(): string
    {
    	$request = $this->datos;

                return 'empresas';
    }

    public function collection()
    {
    	$request = $this->datos;
        $reporte = Empresa::all();
        return $reporte;
    }
}
