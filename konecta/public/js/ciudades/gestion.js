$(document).ready(function() {
    $('#modal_editar').on('shown.bs.modal', function () {
        $("#nombre_ciudad_edit").focus();
    }); 
    $('#modal_crear').on('shown.bs.modal', function () {
        $("#nombre_ciudad").focus();
    });
    $("#ciudad_ciudad, #ciudad_ciudad_edit").select2( {
        language: {
            noResults: function() {return "No hay resultados";},
            searching: function() {return "Buscando..";}
        },
        placeholder: "Seleccione..."
    });
    $('#form_crear').on('submit', function (e) {
        if (e.isDefaultPrevented()) {
        }else{
            e.preventDefault();
            var urlA = baseUrl + "/ciudades";
            jQuery.ajax({
                url: urlA,
                method: 'post',
                data: $('#form_crear').serialize(),
                beforeSend: function() {
                },
                success: function(result){
                    if(result == 0){
                        swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'Ocurrio un problema al guardar',
                            footer: '',
                            confirmButtonText:
                            '<i class="fa fa-check"></i> Aceptar!',
                        });
                    }
                    swal.fire({
                        title: '<strong>La ciudad ha sido creada</strong>',
                        type: 'success',
                        html: 'Gracias.',
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                        '<i class="fa fa-check"></i> Aceptar!',
                    });
                    location.reload();
                },
                error: function(result){
                    var lista = "<ul>";
                    jQuery.each(result.responseJSON.errors, function(key, value){
                        lista += "<li>" + value + "</li>";
                    });
                    lista += "</ul>";
                    swal.fire({
                        type: 'error',
                        title: 'Error',
                        html: lista,
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                        '<i class="fa fa-check"></i> Aceptar!',
                    });
                }
            });
        }
    });
    $('#form_actualizar').on('submit', function (e) {
        if (e.isDefaultPrevented()) {
        }else{
            e.preventDefault();
            var urlA = baseUrl + "/ciudades/" + $('#id_ciudad').val();
            jQuery.ajax({
                url: urlA,
                type: 'PUT',
                data: $('#form_actualizar').serialize(),
                beforeSend: function() {
                },
                success: function(result){
                    if(result == 0){
                        swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'Ocurrio un problema al guardar',
                            footer: '',
                            confirmButtonText:
                            '<i class="fa fa-check"></i> Aceptar!',
                        });
                    }
                    $("#form_actualizar").find('input:text, select').val('');
                    swal.fire({
                        title: '<strong>La ciudad ha sido actualizada</strong>',
                        type: 'success',
                        html: 'Gracias.',
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                        '<i class="fa fa-check"></i> OK',
                    });
                    location.reload();
                },
                error: function(result){
                    var lista = "<ul>";
                    jQuery.each(result.responseJSON.errors, function(key, value){
                        lista += "<li>" + value + "</li>";
                    });
                    lista += "</ul>";
                    swal.fire({
                        type: 'error',
                        title: 'Error',
                        html: lista,
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                        '<i class="fa fa-check"></i> OK!',
                    });
                }
            });
        }
    });

    $(document).on('click', '.edit-modal', function() {
        $('#id_ciudad').val($(this).data('id'));
        $('#nombre_ciudad_edit').val($(this).data('nombreciudad'));
        $('#nit_ciudad_edit').val($(this).data('nitciudad'));
        $('#direccion_ciudad_edit').val($(this).data('direccionciudad'));
        $('#telefono_ciudad_edit').val($(this).data('telefonociudad'));
        $('#sede_ciudad_edit').val($(this).data('sedeciudad'));
        $('#ciudad_ciudad_edit').val($(this).data('ciudadciudad'));
        $('#observaciones_ciudad_edit').val($(this).data('observacionesciudad'));

        var ciudades = $('#ciudad_ciudad_edit').val();
        if (ciudades != null) {
            $('#ciudad_ciudad_edit').val(ciudades).trigger("change"); 
        }
        $('#modal_editar').modal('show');
    });

    $(document).on('click', '.delete-modal', function() {
        var id = $(this).data('id');
        var mensaje = "";
        var boton ="";
        $('#btnAccion').removeClass('btn-danger');
        $('#btnAccion').removeClass('btn-warning');
        $('#btnAccion').addClass('btn-danger');
        boton = "Eliminar";
        swal.fire({
            title: "¿Esta seguro de eliminar esta Ciudad? ",
            text: " Recuerde antes, verificar que no esté vinculada con alguna empresa, proveedor, etc.",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: boton
        }).then(function(e) {
            if(e.value){
                var urlA = baseUrl + "/ciudades/" + id;
                jQuery.ajax({
                    url: urlA,
                    type: 'DELETE',
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function(result){
                        if(result == 0){
                            swal.fire({
                                type: 'error',
                                title: 'Error',
                                text: 'Ocurrio un problema al eliminar',
                                footer: '',
                                confirmButtonText:
                                '<i class="fa fa-check"></i> OK!',
                            });
                        }else{
                            swal.fire({
                                title: '<strong>La ciudad ha sido eliminado</strong>',
                                type: 'success',
                                html: 'Gracias.',
                                showCloseButton: true,
                                focusConfirm: false,
                                confirmButtonText:
                                '<i class="fa fa-check"></i> OK!',
                            });
                        }
                        location.reload();
                    },
                    error: function(result){
                        swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'Ocurrio un problema al eliminar',
                            footer: '',
                            confirmButtonText:
                            '<i class="fa fa-check"></i> Aceptar!',
                        });
                    }
                });
            }
        });
    });
});