function validar() {
  // Obtener nombre de archivo
  let archivo = document.getElementById('archivo').value,
  // Obtener extensión del archivo
      extension = archivo.substring(archivo.lastIndexOf('.'),archivo.length);
  // Si la extensión obtenida no está incluida en la lista de valores
  // del atributo "accept", mostrar un error.
  if(document.getElementById('archivo').getAttribute('accept').split(',').indexOf(extension) < 0) {
    alert('Archivo inválido. No se permite la extensión ' + extension);
  }
}
$(document).ready(function() {
    $("#rol, #rol_act, #empresa, #empresa_act").select2( {
        placeholder: "Seleccione..."
    });
    $('#users_table').DataTable({
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
        },
        destroy: true,
        searching: true,
        responsive: true,
        pagingType: "full_numbers"
    });
    $(document).on('click', '.edit-modal', function() {
        $('#id_act').val($(this).data('id'));
        $('#nombres_act').val($(this).data('name'));
        $('#email_act').val($(this).data('email'));
        $('#cedula_act').val($(this).data('cedula'));
        $('#cargo_act').val($(this).data('cargo'));
        $('#celular_act').val($(this).data('celular'));   
        $('#modal_editar').modal('show');
    });
   


    $(document).on('click','.activarUsuario', function(e){
        var id = $(this).data('id');
        $.ajax({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')     
           },
           url: baseUrl+'/registro/activarUsuario',
           type: 'POST',
           data: {id: id},
           dataType: 'json',
           success:(json)=> {
            swal.fire({
                type: 'success',
                title: 'Realizado',
                html: "",
                showCloseButton: true,
                focusConfirm: false,
                timer:2000,
                confirmButtonText:
                '<i class="fa fa-check"></i> Aceptar!',
            });
            location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            swal.fire({
                type: 'error',
                title: 'Error!!, por favor intente nuevamente.',
                html: "",
                showCloseButton: true,
                focusConfirm: false,
                timer:2000,
                confirmButtonText:
                '<i class="fa fa-check"></i> Aceptar!',
            });
        }   
    }) 
    });

    $('#form_crear').on('submit', function (e) {
        if (e.isDefaultPrevented()) {
        }else{
            e.preventDefault();
            var urlA = baseUrl + "/registro";
            jQuery.ajax({
                url: urlA,
                method: 'post',
                data: $('#form_crear').serialize(),
                success: function(result){
                    if(result == 0){
                        swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'Ocurrio un problema al guardar',
                            footer: '',
                            confirmButtonText:
                            '<i class="fa fa-check"></i> Aceptar!',
                        });
                    }
                    $("#form_actualizar").find('input:text, select').val('');
                    swal.fire({
                        title: '<strong>Registro creado Correctamente</strong>',
                        type: 'success',
                        html: 'Gracias.',
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                        '<i class="fa fa-check"></i> Aceptar!',
                    });
                    location.reload();
                },
                error: function(result){
                    var lista = "<ul>";
                    jQuery.each(result.responseJSON.errors, function(key, value){
                        lista += "<li>" + value + "</li>";
                    });
                    lista += "</ul>";
                    swal.fire({
                        type: 'error',
                        title: 'Error',
                        html: lista,
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                        '<i class="fa fa-check"></i> Aceptar!',
                    });
                }
            });
        }
    });
    $('#form_actualizar').on('submit', function (e) {
        if (e.isDefaultPrevented()) {
        }else{
            e.preventDefault();
            var urlA = baseUrl + "/registro/" + $('#id_act').val();
            jQuery.ajax({
                url: urlA,
                type: 'PUT',
                data: $('#form_actualizar').serialize(),
                success: function(result){
                    if(result == 0){
                        swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'Ocurrio un problema al guardar',
                            footer: '',
                            confirmButtonText:
                            '<i class="fa fa-check"></i> Aceptar!',
                        });
                    }
                    $("#form_actualizar").find('input:text, select').val('');
                    swal.fire({
                        title: '<strong>Datos actualizados Correctamente</strong>',
                        type: 'success',
                        html: 'Gracias.',
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                        '<i class="fa fa-check"></i> Aceptar!',
                    });
                    location.reload();
                },
                error: function(result){
                    var lista = "<ul>";
                    jQuery.each(result.responseJSON.errors, function(key, value){
                        lista += "<li>" + value + "</li>";
                    });
                    lista += "</ul>";
                    swal.fire({
                        type: 'error',
                        title: 'Error',
                        html: lista,
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText:
                        '<i class="fa fa-check"></i> Aceptar!',
                    });
                }
            });
        }
    });
    //  insercion masiva
    $('#form_insercion').on('submit', function (e) {
        if (e.isDefaultPrevented()) {
        }else{
            e.preventDefault();
            var file = $('#form_insercion input[type=file]')[0].files[0];
            var form_data = new FormData();
            form_data.append("archivo", file);
            $.each($('#form_insercion').serializeArray(), function(i, field) {
                form_data.append(field.name, field.value);
            });
            var urlA = baseUrl + "/cargue_masivo";
            jQuery.ajax({
                url: urlA,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'POST',
                beforeSend:function() {
                    $("#modal_insercion").modal("hide");
                    showPreload();
                },
                success: function(result){
                    hiddenPreload();
                    if(result == 0){
                        swal({
                            type: 'error',
                            title: 'Error',
                            text: 'Ocurrio un problema al cargar el archivo',
                            footer: '',
                            confirmButtonText: '<i class="fa fa-check"></i> Aceptar!',
                        });
                        datatable.reload();

                    }else{
                        swal({
                            title: '<strong>Se realizo el cargue masivo</strong>',
                            type: 'success',
                            text: result,
                            showCloseButton: true,
                            focusConfirm: false,
                            confirmButtonText: '<i class="fa fa-check"></i> Aceptar!',
                        });
                        datatable.reload();
                    }
                    $("#modal_insercion").modal("hide");
                    $("#form_insercion").find('input:text, select, file').val('');
                    datatable.reload();
                },
                error: function(result){
                    swal({
                        type: 'error',
                        title: 'Error',
                        text: 'Ocurrio un problema al cargar el archivo',
                        showCloseButton: true,
                        focusConfirm: false,
                        confirmButtonText: '<i class="fa fa-check"></i> Aceptar!',
                    });
                }
            });
        }
    });
});