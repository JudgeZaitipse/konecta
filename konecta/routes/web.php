<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'],function () {
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/home', 'HomeController@index')->name('home');
	
	// editar perfil
	Route::post('/users/actualizar/{id}', 'UserController@actualizar')->name('actualizar');
	Route::post('/users/activarUsuario', 'UserController@destroy')->name('activarUsuario');
	// reportes
	Route::post('/users/reporte', 'UserController@reporteUsuarios')->name('reporteUsuarios');
	Route::post('/empresas/reporte', 'EmpresaController@reporteEmpresas')->name('reporteEmpresas');


	// registro usuarios
    Route::post('/registro/activarUsuario', 'RegistroController@destroy')->name('activarUsuario_registro');
   
    //caraga masiva 
    Route::post('/cargue_masivo','RegistroController@insercionMasiva');


	//cargar datatable del lado del servidor
	Route::get('/users/usersIndex','UserController@usersIndex')->name('usersIndex');
	Route::get('/permisos/permissionsIndex','PermissionController@permissionsIndex')->name('permissionsIndex');
	Route::get('/ciudades/ciudadesIndex','CiudadController@ciudadesIndex')->name('ciudadesIndex');
	Route::get('/empresas/empresasIndex','EmpresaController@empresasIndex')->name('empresasIndex');
	
	
	
	//resource
    Route::resource('registro', 'RegistroController');
	Route::resource('users', 'UserController');
	Route::resource('roles', 'RoleController');
	Route::resource('permisos', 'PermissionController');
	Route::resource('empresas', 'EmpresaController');
	Route::resource('ciudades', 'CiudadController');
















});