<?php

use App\Models\Ciudad;
use Illuminate\Database\Seeder;

class CiudadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'nombre' => 'Bogota',
                'created_at' => '2019-06-20 00:08:20',
                'updated_at' => '2019-06-20 00:08:20'
            ],
            [
                'nombre' => 'Medellin',
                'created_at' => '2019-06-20 00:08:20',
                'updated_at' => '2019-06-20 00:08:20'
            ]
        ];

        foreach ($users as $key => $value) {
            Ciudad::create($value);
        }
    }
}
