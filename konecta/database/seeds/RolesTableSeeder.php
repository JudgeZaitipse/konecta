<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Administrador',
                'description' => 'Administrador'
            ],
            [
                'name' => 'vendedor',
                'display_name' => 'Vendedor',
                'description' => 'Vendedor'
            ]
        ];

        foreach ($roles as $key => $value) {
            Role::create($value);
        }
    }
}
