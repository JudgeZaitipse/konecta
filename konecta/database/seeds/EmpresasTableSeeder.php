<?php

use App\Models\Empresa;
use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                $users = [
            [
                'nombre' => 'Konecta',
                'nit' => '11111111',
                'direccion' => 'Calle 45 # 6-25',
                'telefono' => 23245434,
                'ciudad_id' => 1,
                'estado' => 1,
                'observaciones' => 'Ninguna',
                'created_at' => '2019-06-20 00:08:20',
                'updated_at' => '2019-06-20 00:08:20'
            ]
        ];

        foreach ($users as $key => $value) {
            Empresa::create($value);
        }
    }
}
