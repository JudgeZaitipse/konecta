@extends('layouts.app')
@section('titulo')Ciudades @endsection
@section('content')
<div class="card" style="padding: 30px;">
    <div class="">
        @ability('admin', 'crear_ciudad')
        <div class=" pull-left">
            <a href="#" data-toggle="modal" data-target="#modal_crear" class="btn btn-dark btn-elevate btn-pill" title="Crear">
                <span><i class="la la-plus"></i><span>Agregar Ciudad</span></span>
            </a>
        </div>
        @endability
    </div>
    <div class="card-body">
        <table id="ciudades_table" class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" style="width:100%">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
        @include('ciudades.partials.modals')
    </div>
</div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#ciudades_table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[ 1, "asc" ]],
            "ajax": {
                "url":"{{ route('ciudadesIndex') }}",
                "dataType":"json",
                "type":"GET",
                "data":{"_token":"{{ csrf_token() }}"}
            },
            columns: [{
                data: "id",
                title: "id",
                width: 100,
                height:100,
            },{
                data: "nombre",
                title: "nombre",
                width: 100,
                height:100,
            },{
                data: "action",
                title: "Acciones",
                width: 100,
                height:100,
                render: function (data,type,full,meta) {
                    return ` @ability('admin', 'editar_ciudad')
                    <div class='btn-group'>
                    <a href="javascript:void(0)" class="edit-modal btn btn-brand  m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" 
                    data-id="${data.id}" 
                    data-nombreciudad="${data.nombre}" 
                    title="Editar">
                    <i class="fa flaticon-edit-1"></i>
                    </a>
                    @endability
                    @ability('admin', 'eliminar_ciudad')
                    <a href="javascript:void(0)" class="delete-modal btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" data-id="${data.id}" title="Eliminar">
                    <i class="fa flaticon-delete-1"></i>
                    </a>
                    </div>
                    @endability`;

                },
                orderable: false
            }],
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
            responsive: true,
            pagingType: "full_numbers"
        });
    });
</script>
<script src="{{ asset('js/ciudades/gestion.js') }}"></script>
@endsection