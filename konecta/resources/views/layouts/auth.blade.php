<!DOCTYPE html>
<html lang="es" >
<!-- begin::Head -->
<head>
    <!--begin::Base Path (base relative path for assets of this page) -->
    <base href="../../../../">

    <!--end::Base Path -->
    <meta charset="utf-8" />
        <title> KONECTA | @yield("titulo")</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->

    <style>
        label.error{
            color:#fd397a;
        }
    </style>

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{!! asset('assets/css/demo1/pages/general/login/login-1.css') !!}" rel="stylesheet" type="text/css" />

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{!! asset('assets/css/demo1/style.bundle.css') !!}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{!! asset('assets/css/demo1/skins/header/base/light.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('assets/css/demo1/skins/header/menu/light.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('assets/css/demo1/skins/brand/dark.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('assets/css/demo1/skins/aside/dark.css') !!}" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->
    <link rel="shortcut icon" href="{{ url('image/logos/logo.png') }}" />

</head>
<!-- end::Head -->
<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

            <!--begin::Aside-->
            <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({!! asset('image/bg/bg-7.jpg') !!});">
                <div class="kt-grid__item">
                    <div  class="kt-login__logo" style="float: right;">
                        <img src="{!! asset('image/bg/logo.png') !!}" style="width:200px; height: auto;">
                    </div>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                    <div class="kt-grid__item kt-grid__item--middle">
                        <h3 class="kt-login__title">Bienvenido a KONECTA!</h3>
                        <div style="color: #f9f9fb; padding-top: 25px">
                            Soluciones a medida para la mejor experiencia cliente BPO y Contact Center
                        </div>
                    </div>
                </div>
                <div class="kt-grid__item">
                    <div class="kt-login__info">
                        <div class="kt-login__copyright" style="padding-top: 20px;">
                            &copy {{Date('Y')}} KONECTA
                        </div>
                        <div class="kt-login__copyright" style="float: right;">
                            <a href="https://www.grupokonecta.com/" target="blank" style="float: right;">
                        </div>    
                    </div>
                </div>
            </div>

            <!--begin::Aside-->

            @yield('content')
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin:: Global Mandatory Vendors -->
<script src="{!! asset('assets/vendors/general/jquery/dist/jquery.js') !!}" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script src="{!! asset('assets/vendors/general/popper.js/dist/umd/popper.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/vendors/general/bootstrap/dist/js/bootstrap.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/vendors/general/js-cookie/src/js.cookie.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/vendors/general/moment/min/moment.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/vendors/general/sticky-js/dist/sticky.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('assets/vendors/general/wnumb/wNumb.js') !!}" type="text/javascript"></script>

<!--end:: Global Mandatory Vendors -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{!! asset('assets/js/demo1/scripts.bundle.js') !!}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<!--begin::Page Scripts(used by this page) -->
<script src="{!! asset('assets/js/demo1/pages/login/login-1.js') !!}" type="text/javascript"></script>
<!--end::Page Scripts -->

<!--begin::Page Snippets -->
<script src="{{asset('js/login.js')}}" type="text/javascript"></script>

<!--end::Page Snippets -->
</body>
<!-- end::Body -->
</html>