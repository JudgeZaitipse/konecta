<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
            <ul class="kt-menu__nav ">
                @ability('admin','mostrar_usuario')
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-statistics"></i><span class="kt-menu__link-text">Administración</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu"><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Administración</span></span></li>
        
                                    @ability('admin', 'mostrar_usuario')
                                    <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('users.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Usuarios</span></a></li>
                                    @endability
                                    @ability('admin', 'mostrar_rol')
                                    <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('roles.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot fa fa-rol"><span></span></i><span class="kt-menu__link-text">Roles</span></a></li>
                                    @endability
                                    @ability('admin', 'mostrar_permiso')
                                    <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('permisos.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot fa fa-rol"><span></span></i><span class="kt-menu__link-text">Permisos</span></a></li>
                                    @endability
                                    @ability('admin', 'mostrar_empresa')
                                    <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('empresas.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Empresas</span></a></li>
                                    @endability
                                    @ability('admin', 'mostrar_ciudad')
                                    <li class="kt-menu__item " aria-haspopup="true"><a href="{{route('ciudades.index')}}" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Ciudades</span></a></li>
                                    @endability
                            </ul>
                        </div>
                    </li>
                @endability    
                    {{-- clientes --}}
                @ability('admin,vendedor','mostrar_clientes')
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                        <a href="{{route('registro.index')}}" class="kt-menu__link kt-menu__toggle">
                            <i class="kt-menu__link-icon fa flaticon-line-graph"></i>
                            <span class="kt-menu__link-text">Clientes</span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                    </li>
                @endability 
            </ul>
        </div>
    </div>

    <!-- end:: Aside Menu -->
</div>
<!-- end:: Aside -->