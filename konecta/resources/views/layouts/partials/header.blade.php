<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid kt-grid--ver  kt-header--fixed ">

    <!-- begin:: Aside -->
    <div class="kt-header__brand kt-grid__item  " id="kt_header_brand" style="background-color:#fff;">
        <div class="kt-header__brand-logo">
            <a href="{{ url('/home') }}">
                <img alt="Logo" src="{!! asset('image/logos/logo_3.jpg') !!}" style="width:40px; height: 65px;" />  
            </a>
        </div>
    </div>
    <h3 class="kt-header__title kt-grid__item">
        KONECTA - @yield("titulo")
    </h3>
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
    <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">

        </div>
    </div> 
    <div class="kt-header__topbar">
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                <img class="kt-hidden" alt="Pic" src="{!! asset('image/users/user.png') !!}" />
                <span class="kt-header__topbar-icon kt-hidden-"><i class="flaticon2-user-outline-symbol"></i></span>
            </div>
            
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
                <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url({!! asset('image/bg/450.jpg') !!})">

                    <div class="row">
                        <div class="col-lg-2 kt-user-card__avatar">
                            <img class="kt-hidden" alt="Pic" src="{!! asset('image/users/user.png') !!}" />
                            <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{!! substr(Auth::user()->name,0,1) !!}
                            </span>
                        </div>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="kt-user-card__name">
                                        {{Auth::user()->name }}
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="kt-user-card__name" style="font-size: 12px;">
                                        {{Auth::user()->empresas->nombre }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-notification">
                    <div class="kt-notification__custom kt-space-between">
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Cerrar Sesión</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        @ability('admin', 'actualizar_perfil')
                        <a href="{{ route('users.edit',[Auth::user()->id]) }}" class="btn btn-label btn-label-success btn-sm btn-bold">Editar Pefil</a>
                        @endability
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>

