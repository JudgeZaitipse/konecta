<!DOCTYPE html>
<html lang="es" >
<!-- begin::Head -->
<head>
	<!--begin::Base Path (base relative path for assets of this page) -->
	<base href="../">

	<!--end::Base Path -->
	<meta charset="utf-8" />
	<title> KONECTA | @yield("titulo")</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--begin::Fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->

	<!--end::Page Vendors Styles -->

	<!--begin:: Global Mandatory Vendors -->
	<link href="{!! asset('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css') !!}" rel="stylesheet" type="text/css" />

	<!--end:: Global Mandatory Vendors -->

	<!--begin:: Global Optional Vendors -->

	<link href="{!! asset('css/global.css') !!}" rel="stylesheet" type="text/css" />
	
	<link href="{!! asset('assets/vendors/general/select2/dist/css/select2.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/tether/dist/css/tether.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/animate.css/animate.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/toastr/build/toastr.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/morris.js/morris.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/sweetalert2/dist/sweetalert2.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/socicon/css/socicon.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/custom/vendors/flaticon/flaticon.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/custom/vendors/flaticon2/flaticon.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css" />
	<!--end:: Global Optional Vendors -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="{!! asset('assets/vendors/base/vendors.bundle.css') !!}" rel="stylesheet" type="text/css" />
	<link href="{!! asset('assets/css/demo6/style.bundle.css') !!}" rel="stylesheet" type="text/css" />

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->

	<!--end::Layout Skins -->
	<link rel="shortcut icon" href="{{ url('image/logos/logo_3.png') }}" />
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<style>
		textarea:invalid, input:invalid, select:invalid {
			border: 1px solid red;
		}
		/* preloader */
		$custom-file-text: (
			en: "Browse",
			es: "Elegir"
			);
		.switchasig{
			padding-left: 40%;
			padding-bottom: 0px !important;
			margin-bottom: -41px !important;
		}
		#content {
			margin: 0 auto;
			padding-bottom: 50px;
			width: 80%;
			max-width: 978px;
		}

		/* The Loader */
		#loader-wrapper {
			position: fixed;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			z-index: 100;
			overflow: hidden;
		}
		.no-js #loader-wrapper {
			display: none;
		}

		#loader {
			display: block;
			position: relative;
			left: 50%;
			top: 50%;
			width: 150px;
			height: 150px;
			margin: -75px 0 0 -75px;
			border-radius: 50%;
			border: 3px solid transparent;
			border-top-color: #16a085;
			-webkit-animation: spin 1.7s linear infinite;
			animation: spin 1.7s linear infinite;
			z-index: 11;
		}
		#loader:before {
			content: "";
			position: absolute;
			top: 5px;
			left: 5px;
			right: 5px;
			bottom: 5px;
			border-radius: 50%;
			border: 3px solid transparent;
			border-top-color: #e74c3c;
			-webkit-animation: spin-reverse .6s linear infinite;
			animation: spin-reverse .6s linear infinite;
		}
		#loader:after {
			content: "";
			position: absolute;
			top: 15px;
			left: 15px;
			right: 15px;
			bottom: 15px;
			border-radius: 50%;
			border: 3px solid transparent;
			border-top-color: #f9c922;
			-webkit-animation: spin 1s linear infinite;
			animation: spin 1s linear infinite;
		}

		@-webkit-keyframes spin {
			0% {
				-webkit-transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(360deg);
			}
		}
		@keyframes spin {
			0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(360deg);
				transform: rotate(360deg);
			}
		}
		@-webkit-keyframes spin-reverse {
			0% {
				-webkit-transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(-360deg);
			}
		}
		@keyframes spin-reverse {
			0% {
				-webkit-transform: rotate(0deg);
				transform: rotate(0deg);
			}
			100% {
				-webkit-transform: rotate(-360deg);
				transform: rotate(-360deg);
			}
		}
		#loader-wrapper .loader-section {
			position: fixed;
			top: 0;
			width: 50%;
			height: 100%;
			background: #22222282;
			z-index: 10;
		}

		#loader-wrapper .loader-section.section-left {
			left: 0;
		}

		#loader-wrapper .loader-section.section-right {
			right: 0;
		}

		/* Loaded styles */
		.loaded #loader-wrapper .loader-section.section-left {
			-webkit-transform: translateX(-100%);
			transform: translateX(-100%);
			transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
		}

		.loaded #loader-wrapper .loader-section.section-right {
			-webkit-transform: translateX(100%);
			transform: translateX(100%);
			transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
		}

		.loaded #loader {
			opacity: 0;
			transition: all 0.3s ease-out;
		}

		.loaded #loader-wrapper {
			visibility: hidden;
			-webkit-transform: translateY(-100%);
			transform: translateY(-100%);
			transition: all 0.3s 1s ease-out;
		}
	</style>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize kt-page--loading">

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="{{ url('/home') }}">
					<img alt="Logo" src="{!! asset('image/logos/logo_3.jpg') !!}" style="width:35px; height: 45px;" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<div class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></div>
			<div class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></div>
		</div>
	</div>
	<!-- end:: Header Mobile -->
	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			@include('layouts.partials.menu')

			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
				@include('layouts.partials.header')
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
					@yield('content')
					<!-- end:: Content -->
				</div>

				<!-- begin:: Footer -->
				<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
					<div class="kt-footer__copyright">
						{{Date('Y')}}&nbsp;&copy;&nbsp;<a href="https://www.grupokonecta.com/" target="_blank" class="kt-link">KONECTA</a>
					</div>
				</div>
				<!-- end:: Footer -->
			</div>
		</div>
	</div>

	<!-- end:: Page -->

	<!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	<!-- begin::Scroll Top -->
	<div id="m_scroll_top" class="m-scroll-top">
		<i class="la la-arrow-up"></i>
	</div>

	<!-- begin::Global Config(global config for global JS sciprts) -->
	<script>
		var KTAppOptions = {
			"colors": {
				"state": {
					"brand": "#22b9ff",
					"light": "#ffffff",
					"dark": "#282a3c",
					"primary": "#5867dd",
					"success": "#34bfa3",
					"info": "#36a3f7",
					"warning": "#ffb822",
					"danger": "#fd3995"
				},
				"base": {
					"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
					"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
				}
			}
		};
	</script>

	<!-- end::Global Config -->

	<!--begin:: Global Mandatory Vendors -->
	<script src="{!! asset('assets/vendors/general/jquery/dist/jquery.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('assets/vendors/general/popper.js/dist/umd/popper.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('assets/vendors/general/bootstrap/dist/js/bootstrap.min.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('assets/vendors/general/js-cookie/src/js.cookie.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('assets/vendors/general/moment/min/moment.min.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('assets/vendors/general/sticky-js/dist/sticky.min.js') !!}" type="text/javascript"></script>
	<script src="{!! asset('assets/vendors/general/wnumb/wNumb.js') !!}" type="text/javascript"></script>
	<!--begin::Global Theme Bundle(used by all pages) -->

	<script src="{{asset('assets/vendors/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/demo6/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>

	<script src="{{asset('assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.es.min.js"></script>
	<script src="{{asset('assets/vendors/general/sweetalert2/dist/sweetalert2.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/moment.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js')}}" type="text/javascript"></script>
	<script src="{!! asset('assets/js/demo6/scripts.bundle.js') !!}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}"></script>
	<!--end::Global Theme Bundle -->

	<!--begin::Page Vendors(used by this page) -->

	@stack('scripts')
	<script>
		var baseUrl='{{url('/')}}';
		var showPreload = function (e) {
			$('body').append('<div id="loader-wrapper">' +
				'<div id="loader"></div>' +
				'<div class="loader-section section-left"></div>' +
				'<div class="loader-section section-right"></div>'+
				'</div>');
		}
		var hiddenPreload = function (e) {
			// console.log('Cierre Popup');
			$('#loader-wrapper').remove();
		}
		(function(e,t,n){
			var r=e.querySelectorAll("html")[0];
			r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);

			function validar_numeros(e){
				var tecla = e.keyCode;
				if (tecla==8 || tecla==9 || tecla==13){
					return true;
				}
				if (tecla==69){
					return false;x1
				}
				var patron =/[0-9]/;
				var tecla_final = String.fromCharCode(tecla);
				return patron.test(tecla_final);
			}
			function validar_texto(e){ 
				var tecla = e.keyCode; 
            if (tecla==8) return true; //Tecla de retroceso (para poder borrar) 
            //var patron =/[A-Za-z\áéíóú ÁÉÍÓÚ]/; // Solo acepta letras y vocales acentuadas
            var patron = /[a-zA-ZñÑáéíóúüÁÉÍÓÚÜ\s]+/;
            var te = String.fromCharCode(tecla); 
            return patron.test(te); 
        }
    </script>
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
</body>
<!-- end::Body -->
@yield('scripts')
</html>