<div class="modal fade" id="modal_service_manage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Información Detallada y Gestión del Servicio </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" class="m-form m-form--fit m-form--group-seperator-dashed" id="form_service_manage">
                {{csrf_field()}}
                <div class="modal-body">
                    <div id="div_manage_service"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary m-btn--air" data-dismiss="modal">Cerrar Ventana</button>
                    <a href="#" id="btnManageService" class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--air">
                        <span>
                            <i class="la la-check"></i>
                            <span>Guardar Cambios</span>
                        </span>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->

