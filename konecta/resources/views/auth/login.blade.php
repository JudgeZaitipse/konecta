@extends('layouts.auth')
@section('titulo')Ingresar @endsection
@section('content')
<!--begin::Content-->
<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
    <!--begin::Head-->
    <div class="kt-login__head">
        <!-- <a href="#" class="kt-link kt-login__signup-link">Activar Cuenta</a> -->
    </div>
    <!--end::Head-->
    <!--begin::Body-->
    <div class="kt-login__body">
        <!--begin::Signin-->
        <div class="kt-login__form">
          <div class="kt-grid__item" style="justify-content: center; text-align: center;" >

            <a href="{{ url('/login') }}" class="kt-login__logo">
                <img src="{!! asset('image/users/user.png') !!}" style="width:150px; height: 150px;" >
            </a>
        </div>
        <br>
        <br>
        <div class="kt-login__title">
            <h3>Ingresa tus credenciales</h3>
        </div>
        <!--begin::Form-->
        <form class="kt-form" action="{{route('login')}}" method="post" novalidate="novalidate">
            {{ csrf_field() }}
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Usuario" name="username" autocomplete="off">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" placeholder="Contraseña" name="password">
            </div>

            <!--begin::Action-->
            <div class="kt-login__actions">
                <a href="{{ url('/password/reset') }}" class="kt-link kt-login__link-forgot">
                    Olvidó su contraseña ?
                </a>
                <button id="m_login_signin_submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Iniciar Sesión</button>
            </div>

            <!--end::Action-->
        </form>

        <!--end::Form-->
    </div>

    <!--end::Signin-->
</div>

<!--end::Body-->
</div>
<!--end::Content-->
@endsection
