<!--begin:Modal -->
    <div class="modal fade" id="modal_crear" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form id="form_crear" data-toggle="validator" role ="form" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Agregar Empresa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger" style="display:none">
                            <ul class="list-errores"></ul>
                        </div>
                        {{ csrf_field() }}
                        <!-- Tipo Documento Field -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombre_empresa">Nombre</label>
                                    <input type="text" name="nombre_empresa" id="nombre_empresa" class="form-control" placeholder="Nombre" pattern="[a-zA-ZñÑáéíóúüÁÉÍÓÚÜ 0-9]{4,100}" required onkeypress="return validar_texto(event)">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nit_empresa">NIT</label>
                                    <input type="text" name="nit_empresa" id="nit_empresa" class="form-control" placeholder="NIT" pattern="[0-9]{8,10}"required onkeypress="return validar_numeros(event)" title="numero de NIT icorrecto">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="direccion_empresa">Dirección</label>
                                    <input type="text" name="direccion_empresa" id="direccion_empresa" class="form-control" placeholder="Dirección">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="telefono_empresa">Teléfono</label>
                                    <input type="text" name="telefono_empresa" id="telefono_empresa" class="form-control" placeholder="Telefono" pattern="[0-9]{7,10}" onkeypress="return validar_numeros(event)">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="ciudad_empresa">Ciudad</label>
                                {{ Form::select('ciudad_empresa', $ciudades, null, ['class'=> 'form-control m-select2', 'id'=>'ciudad_empresa', 'required' => 'true', 'style'=>'width:100%;','placeholder'=>'seleccione...']) }}
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="observaciones_empresa">Observaciones</label>
                                    <textarea name="observaciones_empresa" id="observaciones_empresa" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        {{ Form::submit('Registrar', array('class' => 'btn btn-primary','id'=>'crearEmpresa')) }}
                    </div>
                </form>
            </div>
        </div>
    </div>
<!--end:Modal -->
<!--begin:Modal -->
<div class="modal fade" id="modal_editar" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="form_actualizar" data-toggle="validator" role ="form" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Empresa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none">
                        <ul class="list-errores"></ul>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="id_empresa" id="id_empresa">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombre_empresa_edit">Nombre</label>
                                <input type="text" name="nombre_empresa_edit" id="nombre_empresa_edit" class="form-control" placeholder="Nombre" pattern="[a-zA-ZñÑáéíóúüÁÉÍÓÚÜ 0-9]{4,200}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nit_empresa_edit">NIT</label>
                                <input type="text" name="nit_empresa_edit" id="nit_empresa_edit" class="form-control" placeholder="NIT" pattern="[0-9]{8,10}" required onkeypress="return validar_numeros(event)" title="numero de NIT icorrecto">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="direccion_empresa_edit">Dirección</label>
                                <input type="text" name="direccion_empresa_edit" id="direccion_empresa_edit" class="form-control" placeholder="Dirección">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="telefono_empresa_edit">Teléfono</label>
                                <input type="text" name="telefono_empresa_edit" id="telefono_empresa_edit" class="form-control" placeholder="Telefono" pattern="[0-9]{7,10}" onkeypress="return validar_numeros(event)">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="ciudad_empresa_edit">Ciudad</label>
                                {{ Form::select('ciudad_empresa_edit', $ciudades, null, ['class'=> 'form-control m-select2', 'id'=>'ciudad_empresa_edit', 'required' => 'true', 'style'=>'width:100%;','placeholder'=>'seleccione...']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="observaciones_empresa_edit">Observaciones</label>
                                <textarea name="observaciones_empresa_edit" id="observaciones_empresa_edit" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        {{ Form::submit('Actualizar', array('class' => 'btn btn-primary')) }}
                </div>
            </form>
        </div>
    </div>
</div>
<!--end:Modal -->