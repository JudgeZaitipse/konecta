@extends('layouts.app')
@section('titulo')Empresas @endsection
@section('content')
<div class="card" style="padding: 30px;">
    <div class="">
        @ability('admin', 'crear_empresa')
        <div class=" pull-left">
            <a href="#" data-toggle="modal" data-target="#modal_crear" class="btn btn-dark btn-elevate btn-pill" title="Crear">
                <span><i class="la la-plus"></i><span>Agregar Empresa</span></span>
            </a>
        </div>
        @endability
        @ability('admin', 'exportar_empresa')
        <form class="m-form m-form--fit m-form--label-align-right" method="POST" action="{{route('reporteEmpresas')}}" id="myForm">
            {{csrf_field()}}
            <div class=" pull-right">
                <button class="btn btn-success btn-elevate btn-pill" title="Exportar">
                    <span><i class="fas fa-file-excel"></i><span>Exportar</span></span>
                </button>
            </div>
        </form>
        @endability
    </div>
    <div class="card-body">
        <table id="empresas_table" class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" style="width:100%">
            <thead>
                <tr>
                    <th>Empresa</th>
                    <th>NIT</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th>Ciudad</th>
                    <th>Observaciones</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
        @include('empresas.partials.modals')
    </div>
</div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#empresas_table').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[ 0, "asc" ]],
            "ajax": {
                "url":"{{ route('empresasIndex') }}",
                "dataType":"json",
                "type":"GET",
                "data":{"_token":"{{ csrf_token() }}"}
            },
            columns: [{
                data: "nombre",
                title: "Nombre",
                width: 100,
                height:100,
            },{
                data: "nit",
                title: "NIT",
                width: 100,
                height:100,
            },{
                data: "direccion",
                title: "Direccion",
                width: 100,
                height:100,
            },{
                data: "telefono",
                title: "Telefono",
                width: 100,
                height:100,
            },{
                data: "ciudad_id",
                title: "Ciudad",
                width: 100,
                height:100,
            },{
                data: "observaciones",
                title: "Observaciones",
                width: 100,
                height:100,
            },{
                data: "action",
                title: "Acciones",
                width: 100,
                height:100,
                render: function (data,type,full,meta) {
                    return ` @ability('admin', 'editar_empresa')
                    <div class='btn-group'>
                    <a href="javascript:void(0)" class="edit-modal btn btn-brand  m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air"  data-id="${data.id }" data-nombreempresa="${data.nombre}" data-nitempresa="${data.nit }" data-direccionempresa="${data.direccion}" data-telefonoempresa ="${data.telefono}"  data-ciudadempresa="${data.ciudad_id}" data-observacionesempresa="${data.observaciones}" title="Editar">
                    <i class="fa flaticon-edit-1"></i>
                    </a>
                    </div>
                    @endability`;

                },
                orderable: false
            }],
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
            responsive: true,
            pagingType: "full_numbers"
        });
    });
</script>
<script src="{{ asset('js/empresas/gestion.js') }}"></script>
@endsection

