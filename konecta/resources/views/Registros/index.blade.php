@extends('layouts.app')
@section('titulo')Registros @endsection
@section('content')
<div class="card" style="padding: 30px;">
    <div class="">
        @ability('admin', 'mostrar_clientes')
        <div class="pull-left">
            <a href="#" data-toggle="modal" data-target="#modal_crear_user" class="btn btn-dark btn-elevate btn-pill" title="Agregar">
                <span><i class="la la-plus"></i><span>Agregar Cliente</span></span>
            </a>
        </div>
        @endability
        @ability('admin', 'mostrar_clientes')
        <div class="pull-left ml-2">
            <a href="#" data-toggle="modal" data-target="#modal_masivo" class="btn btn-dark btn-elevate btn-pill" title="Agregar">
                <span><i class="la la-plus"></i><span>Carga Masiva Clientea</span></span>
            </a>
        </div>
        @endability
    </div>
    <div class="card-body">
        <table id="users_table" class="table table-striped- table-bordered table-hover table-checkable dataTable no-footer dtr-inline" style="width:100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cèdula</th>
                    <th>Correo</th>
                    <th>Telefono</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($registro as $user)
                <tr>
                    <td>{{ $user->nombres }}</td>
                    <td>{{ $user->cedula }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->celular }}</td>
                    <td>
                        <div class='btn-group'>
                            @ability('admin', 'mostrar_clientes')
                            <a href="javascript:void(0)" class="edit-modal btn btn-brand  m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air"  data-id="{{$user->id}}" data-name="{{ $user->nombres }}" data-cedula="{{ $user->cedula }}" data-email="{{ $user->email }}"  data-cargo="{{ $user->cargo }}"   data-celular="{{ $user->celular }}" title="Editar">
                            <i class="fa flaticon-edit-1"></i>
                        </a>
                        @endability
                        @ability('admin', 'mostrar_clientes')
                        <a href="javascript:void(0)" class="activarUsuario btn btn-danger  m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air" data-id="{{$user->id}}" title="Bloquear">
                            <i class="fas fa-ban"></i> 
                        </a>
                    </div>
                    @endability
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @include('Registros.partials.modals')
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{ asset('js/registros/gestion.js') }}"></script>
@endsection