<!--begin::Modal-->
<div class="modal fade" id="modal_info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titulo_modal_info"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                   <div class="col-md-6" id="name_info">
                       <strong>Nombre:</strong>
                   </div>
                   <div class="col-md-6" id="username_info">
                    <strong>Cédula:</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" id="email_info">
                    <strong>Email</strong>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" id="cargo_info">
                    <strong>Cargo:</strong>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>
<!--end::Modal-->

<!--begin:Modal -->
<div class="modal fade" id="modal_crear_user" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <form id="form_crear" data-toggle="validator" role ="form" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none">
                        <ul class="list-errores"></ul>
                    </div>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombres">Nombre</label>
                                <input type="text" name="nombres" id="nombres" class="form-control" placeholder="Nombre Completo" required data-remote="/users/validarNombre/" data-error="El nombre ya ha sido registrado" onkeypress="return validar_texto(event)" >
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cedula">Cédula</label>
                                <div class="m-input-icon m-input-icon--left">
                                    <input onkeypress="return validar_numeros(event)" type="text" name="cedula" id="cedula" class="form-control m-input" placeholder="Cédula" required >
                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                   <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="celular">Célular</label>
                                <input type="text" name="celular" id="celular" class="form-control m-input" placeholder="Célular" required onkeypress="return validar_numeros(event)">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cargo">Cargo</label>
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" name="cargo" id="cargo" class="form-control m-input" placeholder="Cargo" required>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="email" name="email" id="email" class="form-control m-input" placeholder="Correo electronico" required>
                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-at"></i></span></span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                   </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                {{ Form::submit('Registrar', array('class' => 'btn btn-primary')) }}
            </div>
        </form>
    </div>
</div>
</div>
<!--end:Modal -->

<!--begin:Modal  editar -->
<div class="modal fade" id="modal_editar" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <form id="form_actualizar" data-toggle="validator" role ="form" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Cliente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" style="display:none">
                        <ul class="list-errores"></ul>
                    </div>
                    {{ csrf_field() }}
                    <input type="hidden" name="id_act" id="id_act">
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nombres_act">Nombre</label>
                                <input type="text" name="nombres_act" id="nombres_act" class="form-control" placeholder="Nombre Completo" required  onkeypress="return validar_texto(event)">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="cedula_act">Cédula</label>
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" name="cedula_act" id="cedula_act" class="form-control m-input" placeholder="Cédula" onkeypress="return validar_numeros(event)" required>
                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-user"></i></span></span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>
                   <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="celular_act">Célular</label>
                                <input type="text" onkeypress="return validar_numeros(event)" name="celular_act" id="celular_act" class="form-control m-input" placeholder="Célular" required>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cargo_act">Cargo</label>
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" name="cargo_act" id="cargo_act" class="form-control m-input" placeholder="Cargo" required>
                            </div>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                  <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email_act">Email</label>
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="email" name="email_act" id="email_act" class="form-control m-input" placeholder="Correo electronico" required>
                                    <span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-at"></i></span></span>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                   </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    {{ Form::submit('Actualizar', array('class' => 'btn btn-primary')) }}
                </div>
            </form>
        </div>
    </div>
</div>
<!--end:Modal -->

<!--begin:carga masiva -->
 <div class="modal fade" id="modal_masivo" tabindex="-1" role="dialog" aria-labelledby="modal_crear" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Inserción Masiva</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form id="form_insercion" data-toggle="validator" role ="form" enctype="multipart/form-data">
                    <div class="modal-body">
                        <!-- Empresa Id Field -->
                        <div class="form-group">
                            <label for="">Para descargar la plantilla de clic <a href="{{asset('plantilla/plantilla_insercion_masiva.xlsx')}}">Aqui</a></label>
                        </div>
                        {{ csrf_field() }}
                      
                        <div class="form-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" name="archivo" accept=".xlsx,.xls" id="excel" readonly="required" lang="es">
                              <label class="custom-file-label" data-browse="Subir Archivo" for="excel">Seleccionar Archivo</label>
                            </div>
                        </div>
                        <!-- Cedula Field -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        {{ Form::submit('Registrar', array('class' => 'btn btn-primary')) }}
                    </div>
                </form>
            </div>
        </div>
    </div>
<!--end:Modal  carga masiva-->